
Lab 2: Flow and Congestion Control/Experiment Design
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 2 - Flow and Congestion Control
-------------- | --------------------------------
Name           |
Net ID         |
Report due     | Sunday, 22 February 11:59PM


Please answer the following questions:

1) Describe (as specifically as possible) the *goal* of your
experiment. What makes this a good goal? Do not just repeat
all of the indicators of a "good" goal described in the lecture;
explain *how* your selected goal embodies those indicators.


2) What is the TCP congestion control variant you have selected
as the main subject of your study? Describe briefly (1 paragraph)
what characterizes this TCP congestion control, and how it works.
Cite your sources.

3) What is the other LFN TCP you have selected to use in your
experiment? What is the non-LFN TCP you have selected to use
in your experiment? Be brief (1 paragraph each), and cite your
sources. Why did you choose these specifically?

4) Describe the parameters you have chosen to vary in your
experiment. Why did you choose these? How does this selection help further your stated goal?

5) What metrics are you going to measure in your experiment?
Why did you choose these? How does this selection help further your stated goal?

6) For each **experimental unit** in your experiment, describe:

* The specific parameters with which this experimental unit ran
* The names of all the data files which give results from this experimental unit. Include all of these files in this `submit` folder.
* The specific values of the metrics you have chosen to measure.

7) Describe any evidence of *interactions* you can see in the results of your experiment.

8) Briefly describe the results of your experiment (1-2 paragraphs). You may include images by putting them on an online
image hosting service, then putting the image URL in this file
using the syntax

![](http://link/to/image.png)

9) Find a published paper that studies the same TCP congestion
control variant you have chosen (it may study others as well).
Identify the paper you have chosen with a full citation, and
briefly answer the following questions about it:

 * What research question does this study seek to answer?
 * What kind of network environment was this study conducted in?
 * How representative is the above network environment of the network setting this TCP variant is designed for?
 * Does this study make some comparison to another TCP congestion
 control algorithm? If so, which, and does the author explain why these were selected?
 * What parameters does the author of this study consider? Does the
 author explain why?
 * What metrics does the author of this study consider? Does the author explain why?
 * Critique the experiment(s) in the paper. Does it make any of
 the common mistakes described in the lab lecture? Explain.
