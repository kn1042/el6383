
Lab 3: Routing and resiliency
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 3 - Routing and resiliency
-------------- | --------------------------------
Name           |
Net ID         |
Report due     | Sunday, 29 March 11:59PM


Please answer the following questions:


## Dijkstra's algorithm experiment

1. Were you able to successfully produce experiment results? If so, show a screenshot of your experiment results (topology + completed table).

2. How long did it take you to run this experiment, from start (create an account) to finish (getting a screenshot of results)?

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if there was no web-based interface on hyperion.poly.edu?

## OSPF experiment

1. Were you able to successfully produce experiment results? If so, show your experiment results. You should have:
 * Traceroute from client to server with all links active
 * OSPF table on router-1 with all links active
 * Traceroute from client to server with the router-2 link down
 * OSPF table on router-1 with the router-2 link down

2. How long did it take you to run this experiment, from start (reserve resources on GENI) to finish (getting the output to put in the previous )?

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if the experiment artifacts (disk image, RSpec, etc.) were not provided for you?
